module.exports = {
  simpleTextGet: (url) => fetch(url).then(response => response.text())
};