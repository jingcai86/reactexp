import * as SRD from "storm-react-diagrams";

module.exports = {
  createWhereArchitecture: () => {
      const engine = new SRD.DiagramEngine();
      engine.installDefaultFactories();
  
      const model = new SRD.DiagramModel();

      const motionNode = new SRD.DefaultNodeModel("Motion", "#ff4242");
      const motionPortOut = motionNode.addOutPort(" Motion Data");
      motionNode.setPosition(50, 50);

      const tempNode = new SRD.DefaultNodeModel("Temperature", "#ff4242");
      const tempPortOut = tempNode.addOutPort(" Temperature Data");
      tempNode.setPosition(50, 150);

      const BLENode = new SRD.DefaultNodeModel("BlueTooth", "#ff4242");
      const BLEPortOut = BLENode.addOutPort(" BLE Data");
      BLENode.setPosition(50, 250);

      const sensorsNode = new SRD.DefaultNodeModel("Enlighted Sensors", "#e6bf92");
      const sensorsPortIn= sensorsNode.addInPort(" ");
      const sensorsPortOut = sensorsNode.addOutPort("On Premises");
      sensorsNode.setPosition(300, 50);

      const sensorInLink1 = motionPortOut.link(sensorsPortIn);
      const sensorInLink2 = tempPortOut.link(sensorsPortIn);
      const sensorInLink3 = BLEPortOut.link(sensorsPortIn);
  
      const gatewayNode = new SRD.DefaultNodeModel("Gateways (For an Area)", "#70d09d");
      const gatewayPortIn = gatewayNode.addInPort(" ");
      const gatewayPortOut = gatewayNode.addOutPort("On Premises Or Cloud");
      gatewayNode.setPosition(300, 150);

      const sensorLink = sensorsPortOut.link(gatewayPortIn);

      const emNode = new SRD.DefaultNodeModel("Energy Manager (For an Entire Floor)", "#70d09d");
      const emPortIn = emNode.addInPort(" ");
      const emPortOut = emNode.addOutPort("On Premises Or Cloud");
      emNode.setPosition(300, 250);

      const gatewayLink = gatewayPortOut.link(emPortIn);

      const whereKafkaNode = new SRD.DefaultNodeModel("Where Kafka", "#7febff");
      const whereKafkaPortIn = whereKafkaNode.addInPort(" ");
      const whereKafkaPortOut = whereKafkaNode.addOutPort("Data Pipeline");
      whereKafkaNode.setPosition(400, 350);

      const whereLink1 = emPortOut.link(whereKafkaPortIn);

      const whereStormNode = new SRD.DefaultNodeModel("Where Storm", "#7febff");
      const whereStormPortIn = whereStormNode.addInPort(" ");
      const whereStormPortOut = whereStormNode.addOutPort("Storm Topology");
      whereStormNode.setPosition(530, 350);

      const whereLink2 = whereKafkaPortOut.link(whereStormPortIn);

      const whereApiNode = new SRD.DefaultNodeModel("Where API", "#7febff");
      const whereApiPortIn = whereApiNode.addInPort(" ");
      const whereApiPortOut = whereApiNode.addOutPort("Serve content");
      whereApiNode.setPosition(670, 350);

      const whereLink3 = whereStormPortOut.link(whereApiPortIn);

      const whereWebClient = new SRD.DefaultNodeModel("Client Side - Web", "#25e42f");
      const whereWebClientPortIn = whereWebClient.addInPort("Web Browser");
      whereWebClient.setPosition(850, 290);

      const whereMobileClient = new SRD.DefaultNodeModel("Client Side - Mobile", "#25e42f");
      const whereMobileClientPortIn = whereMobileClient.addInPort("Android, iPhone App ");
      whereMobileClient.setPosition(850, 350);

      const cloudNode = new SRD.DefaultNodeModel("Cloud Platform", "#5b42fb");
      const cloudPortIn = cloudNode.addInPort(" ");
      const cloudPortOut = cloudNode.addOutPort("Management");
      cloudNode.setPosition(650, 250);

      const cloudLink = emPortOut.link(cloudPortIn);
      const whereLink4 = whereStormPortOut.link(cloudPortIn);
      const whereLink5 = whereApiPortIn.link(cloudPortOut);
      const whereLink6 = whereApiPortOut.link(whereWebClientPortIn);
      const whereLink7 = whereApiPortOut.link(whereMobileClientPortIn);
      const whereLink8 = cloudPortOut.link(whereWebClientPortIn);
      const whereLink9 = cloudPortOut.link(whereMobileClientPortIn);

      const dbNode = new SRD.DefaultNodeModel("Cassandra", "#5b42fb");
      const dbPort = dbNode.addOutPort("Data Store");
      const dbPortUnused = dbNode.addInPort(" ");
      dbNode.setPosition(500, 180);

      const dbLink = cloudPortIn.link(dbPort);

      const SAMLNode = new SRD.DefaultNodeModel("SAML Authentication", "#9585ff");
      const SAMLPort = SAMLNode.addOutPort("Google, Microsoft...");
      const SAMLPortUnused = SAMLNode.addInPort(" ");
      SAMLNode.setPosition(500, 50);

      const SAMLLink = cloudPortIn.link(SAMLPort);

      model.addAll(
        motionNode, tempNode, BLENode,
        sensorInLink1, sensorInLink2, sensorInLink3,
        sensorsNode, gatewayNode, emNode, cloudNode, dbNode, SAMLNode,
        sensorLink, gatewayLink, cloudLink, dbLink, SAMLLink,


        whereKafkaNode, whereStormNode, whereApiNode,
        whereLink1, whereLink2, whereLink3, whereLink4, whereLink5,
        whereWebClient, whereMobileClient,
        whereLink6,whereLink7,whereLink8,whereLink9
      );
  
      model.setLocked(true);
      
      engine.setDiagramModel(model);
      return engine;
  },
  createWhereUIStructure: () => {
    // 1) setup the diagram engine
    const engine = new SRD.DiagramEngine();
    engine.installDefaultFactories();

    const model = new SRD.DiagramModel();

    const cloudNode = new SRD.DefaultNodeModel("Cloud Platform API", "#5b42fb");
    const cloudPortIn = cloudNode.addInPort(" ");
    const cloudOut3 = cloudNode.addOutPort(" Org/Facility Detail");
    const cloudOut1 = cloudNode.addOutPort(" User Detail");
    const cloudOut2 = cloudNode.addOutPort(" Jwt Token Management");
    cloudNode.setPosition(50,100);

    const wherePlatformNode = new SRD.DefaultNodeModel("Where Platform", "#7febff");
    const whereAPlatformIn = wherePlatformNode.addInPort(" ");
    const whereAPIOut1 = wherePlatformNode.addOutPort(" Tag Detail / Category / Status - API");
    const whereWSOut = wherePlatformNode.addOutPort(" Real Time Tag Position - WebSocket");
    const whereAPIOut2 = wherePlatformNode.addOutPort(" Tag History - API");
    wherePlatformNode.setPosition(100, 250);


    const uiCacheNode = new SRD.DefaultNodeModel("UI Cache Store", "rgb(192,255,0)");
    const uiStoreIn = uiCacheNode.addInPort(" ");
    const uiTagStoreOut = uiCacheNode.addOutPort("Tag Detail Store ");
    const uiCateStoreOut = uiCacheNode.addOutPort("Tag Category Store ");
    const uiStatStoreOut = uiCacheNode.addOutPort("Tag Status Store ");
    uiCacheNode.setPosition(450, 150);

    const uiServiceNode = new SRD.DefaultNodeModel("UI Services", "rgb(192,255,0)");
    const uiService1In = uiServiceNode.addInPort("Authentication Service ");
    const uiService2In = uiServiceNode.addInPort("Sync Service ");
    const uiService1Out = uiServiceNode.addOutPort(" ");
    uiServiceNode.setPosition(450, 250);


    const uiMapNode = new SRD.DefaultNodeModel("UI Mapping", "rgb(192,255,0)");
    const uiMapEngineIn = uiMapNode.addInPort("Render Engine ");
    const uiMapEngineOut = uiMapNode.addOutPort(" ");
    uiMapNode.setPosition(650, 200);

    const uiCurListNode = new SRD.DefaultNodeModel("UI Right Panel List/Detail", "rgb(192,255,0)");
    const uiCurListIn = uiCurListNode.addInPort("From Current Map View ");
    uiCurListNode.setPosition(780, 120);

    const uiTableNode = new SRD.DefaultNodeModel("UI Admin Management", "rgb(192,255,0)");
    const uiTableIn = uiTableNode.addInPort("CURD operations ");
    uiTableNode.setPosition(500, 50);

    const uiReplayNode = new SRD.DefaultNodeModel("UI Tag History Replay", "rgb(192,255,0)");
    const uiReplayIn = uiReplayNode.addInPort("With Analytics ");
    uiReplayNode.setPosition(800, 300);


    const link1 = uiTagStoreOut.link(uiMapEngineIn);
    const link2 = uiCateStoreOut.link(uiMapEngineIn);
    const link3 = uiStatStoreOut.link(uiMapEngineIn);
    const link4 = whereWSOut.link(uiService2In);
    const link5 = uiMapEngineOut.link(uiCurListIn);

    const link6 = uiService1Out.link(uiMapEngineIn);

    const link7 = whereAPIOut1.link(uiStoreIn);
    const link8 = cloudOut3.link(uiStoreIn);
    const link9 = cloudOut1.link(uiService1In);

    const link10 = whereAPIOut1.link(uiTableIn);
    const link11 = whereAPlatformIn.link(cloudOut2);
    const link12 = uiMapEngineOut.link(uiReplayIn);
    const link13 = whereAPIOut2.link(uiReplayIn);
   


    // 6) add the models to the root graph
    model.addAll(
      cloudNode, wherePlatformNode, uiCacheNode, uiServiceNode, uiMapNode, uiCurListNode, uiTableNode, uiReplayNode,
      link1, link2, link3, link4, link5, link6, link7, link8, link9, link10, link11, link12, link13
    );

    model.setLocked(true);

    // 7) load model into engine
    engine.setDiagramModel(model);
    return engine;
  }

}