import XLSX from 'xlsx';
import { message } from 'antd';
import moment from 'moment'

module.exports = {
  readXlsxToJson: (f, callback) => {
    const reader = new FileReader();

    reader.onload = (evt) => {
      /* Parse data */
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, {type:'binary', cellDates:true});
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];


      // https://github.com/SheetJS/js-xlsx/issues/640
      for (let cellref in ws){
        const c = ws[cellref];
        if(c.t==="d"){
            c.v = moment(c.v).format('MM/DD/YYYY');
            //c.w = c.v;
        }
      }

      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {header:1});

      // return
      callback(data);
    };
    reader.readAsBinaryString(f);
  },


  checkXlsxFile : (file) => {
    const isExcelFile = file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';  //can be sitting in a constant store
    if (!isExcelFile) {
      message.error('This is not a xlsx file');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('File must be smaller than 2MB!');
    }
    return isExcelFile && isLt2M;
  },

  writeToXlsxFromJson: (json, filename, attrname, val) => {
    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(json, {skipHeader: 1});

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Worksheet");

    /* generate an XLSX file */
    const filteredName = filename.substring(0, filename.indexOf('.xlsx')) + "-" + attrname + "-" + val + '.xlsx';
    XLSX.writeFile(wb, filteredName);
  }
};