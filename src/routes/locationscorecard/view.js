import React, { Component } from 'react';
import ProjectTemplate from '../../components/project-template/template.component';
import DiagramService from '../../services/diagram.service';

export default class LocationScoreCardDemo extends Component {
  constructor(){
    super();
    this.state = {
      projectDetail: {
        title: "WeWork Location Scorecard",
        subTitle: "A Score System for Wework to Purchase profitable buildings",
        note: "A Project that provides insight on building profitability",
        productUrl: require('../../assets/S-1.pdf'),
        productDescription: "WeWork S1 Product Description",
        media: require('../../assets/video/locationscorecard.mov'),
        tabItems: [
          {
            name: "Purpose",
            image: require("../../assets/images/purpose.png"),
            content: {
              title: "Purpose",
              text: require("../../assets/code/locationscorecard1.txt")
            },
            type: "plain"
          },
          {
            name: "Architecture",
            image: require("../../assets/images/architecture.png"),
            content: {
              title: "Architecture",
              text: require("../../assets/code/locationscorecard3.txt")
            },
            type: "plain"
          },
          {
            name: "UI Stack",
            image: require("../../assets/images/ui_stack.png"),
            content: [
              {
                name: "Framework",
                items: ["React","Ant Design"]
              },
              {
                name: "Mapping",
                items: ["Leaflet"]
              },
              {
                name: "Charting",
                items: ['highchart']
              },
              {
                name: "DB",
                items: ['postgres', 'postGIS', 'arcGIS']
              },
              {
                name: "Others",
                items: ['Kubenetes', 'CircleCI', 'i18n']
              }
            ],
            type: "collapse"
          },
          {
            name: "Map JS (Example)",
            image: require("../../assets/images/purpose.png"),
            content: require("../../assets/code/locationscorecard2.txt"),
            type: "jscode"
          }
        ]
      }
    }
  }
  componentDidMount () {
    parent.postMessage("ready","*");
  }

  render () {
    return (
      <div>
          <ProjectTemplate detail = {this.state.projectDetail}/>
      </div>
    );
  }
}