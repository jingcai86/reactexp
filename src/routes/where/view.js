import React, { Component } from 'react';
import ProjectTemplate from '../../components/project-template/template.component';
import DiagramService from '../../services/diagram.service';

export default class WhereDemo extends Component {
  constructor(){
    super();
    this.state = {
      projectDetail: {
        title: "Enlighted Where",
        subTitle: "Versatile Indoor Location Tracking System",
        note: "A real time tracking system with BLE device with Sensor",
        productUrl: "https://enlighted.estoreco.com/files/ELCS-WSS-SS.pdf",
        productDescription: "Enlighted Where Application Product Manual Link",
        media: require('../../assets/video/where.mov'),
        tabItems: [
          {
            name: "Purpose",
            image: require("../../assets/images/purpose.png"),
            content: {
              title: "Purpose",
              text: require("../../assets/code/where1.txt")
            },
            type: "plain"
          },
          {
            name: "Architecture",
            image: require("../../assets/images/architecture.png"),
            content: {
              diagramEngine: DiagramService.createWhereArchitecture(),
              allowCanvasZoom: false,
              allowLooseLinks: false,
              allowCanvasTranslation: true
            },
            type: "diagram"
          },
          {
            name: "UI Structure",
            image: require("../../assets/images/ui_structure.png"),
            content: {
              diagramEngine: DiagramService.createWhereUIStructure(),
              allowCanvasZoom: false,
              allowLooseLinks: false,
              allowCanvasTranslation: true
            },
            type: "diagram"
          },
          {
            name: "UI Stack",
            image: require("../../assets/images/ui_stack.png"),
            content: [
              {
                name: "Framework",
                items: ["Angular","Material Design"]
              },
              {
                name: "Mapping",
                items: ["Openlayers", "Google Map", "WebGL"]
              },
              {
                name: "Charting",
                items: ['d3js', 'c3js']
              },
              {
                name: "Testing",
                items: ['mocha', 'karma', 'jasmine', 'chai', 'phamtomjs', 'eslint']
              },
              {
                name: "Others",
                items: ['Sockette-WebSocket', 'Moment']
              }
            ],
            type: "collapse"
          },
          {
            name: "Example JS",
            image: require("../../assets/images/purpose.png"),
            content: require("../../assets/code/where2.txt"),
            type: "jscode"
          }
        ]
      }
    }
  }
  componentDidMount () {
    parent.postMessage("ready","*");
  }
  
  render () {
    return (
      <div>
          <ProjectTemplate detail = {this.state.projectDetail}/>
      </div>
    );
  }
}