import React, { Component } from 'react';
import { Upload, Icon, Radio, Divider, Input, Button } from 'antd';

const RadioGroup = Radio.Group;

import ExcelService from '../../services/excel.service';

import './style.scss';



export default class ExcelHelper extends Component {
  constructor(){
    super();
    this.state = {
      filename: '',
      loading: false,
      excelJSON: [],
      selectedRadio: 0,

      emailSubject: "",
      emailContent: ""
    };
  }

  beforeUpload(file) {
    const result = ExcelService.checkXlsxFile(file);

    // if reject, erase file name state
    if (!result) {
      this.setState({ filename: "" });
    } else {
      this.setState({ loading: true });
      this.getXlsxFile(file);
    }
    return false;
  }

  getXlsxFile(f) {
    ExcelService.readXlsxToJson(f, this.processXlsxFile.bind(this));
    this.setState({ 
      filename: f.name,
      loading: false
    });
  }

  processXlsxFile(excelJSON) {
    this.setState({ excelJSON });
  }

  
  onRadioChange = (e) => {
    this.setState({
      selectedRadio: e.target.value,
    },() => {

      // deep copy
      let target = JSON.parse(JSON.stringify(this.state.excelJSON));

      // remove first element
      const titles = target.shift();

      const ret = target.reduce((total, cur) => {
        if (!total[cur[this.state.selectedRadio]]) {
          total[cur[this.state.selectedRadio]] = [cur.slice(0, titles.length)];
          return total;
        } else {
          total[cur[this.state.selectedRadio]].push(cur.slice(0, titles.length));
          return total;
        }
      }, []);

      for(let type in ret) {
        // put back the first element - title
        ret[type].unshift(titles);

        ExcelService.writeToXlsxFromJson(ret[type],this.state.filename,titles[this.state.selectedRadio], type);
      }
    });
  }

  generateEmail() {
    window.open("mailto:?subject=" + this.state.emailSubject + "&body=" + this.state.emailContent, '_self');
  }

  
  render () {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload .xlsx</div>
      </div>
    );

    let titles = "";
    if(this.state.excelJSON.length > 0 && Array.isArray(this.state.excelJSON[0])) {
      titles = this.state.excelJSON[0].map((title, idx) => {
        return (
          <Radio key = {idx} value = {idx}>
            {title}
          </Radio>
        );
      });
    }
    
    return (
      <div id="excel-helper-container">
        <h2>Excel Splitter</h2>
        <div id="excel-top-container">
          <div>
          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action=""
            beforeUpload={this.beforeUpload.bind(this)}
          >
            {uploadButton}
          </Upload>
          </div>
          <div id="excel-description-container">
            <span>
              This is a little tool to split Excel worksheet by grouping entries in one column. For example, you have a column that has entries A,B,C, it will split into 3 files with each file only contains the column that's group by one Entry.
            </span>
            <a href={require('../../assets/Excel Filter.xlsx')} >Sample File</a>
          </div>
        </div>
        
        <Divider orientation="left">Column to Split: </Divider>
        <div id="excel-result-area">
          <RadioGroup onChange={this.onRadioChange} value={this.state.selectedRadio}>
            {titles}
          </RadioGroup>
          <div id="excel-email-area">
            Email Template:
            <Input placeholder="Email Subject" onChange={e => this.setState({ emailSubject: e.target.value })}/>
            <div style={{ margin: '1.5rem 0' }} />
            <Input.TextArea placeholder="Email Content" rows={10} onChange={e => this.setState({ emailContent: e.target.value })}/>
            <div style={{ margin: '1.5rem 0' }} />
            <Button type="primary" onClick={this.generateEmail.bind(this)}>Generate Email</Button>
          </div>
        </div>
      </div>
    );
  }
};
