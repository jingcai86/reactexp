import React, { Component } from 'react';
import './app.scss';
import RouteDef from './route.def';

class ReactExp extends Component {
  render() {
    return (
      <div>
        <RouteDef />
      </div>
    );
  }
}

function A(props) {
  // you can use object spread because babel-preset-react-app is set up for you
  const { href, children, ...rest } = props;
  return (
    <a
      className="App-link"
      href={href}
      target="_blank"
      rel="noopener noreferrer"
      {...rest}
    >
      {children}
    </a>
  );
}
export default ReactExp;
