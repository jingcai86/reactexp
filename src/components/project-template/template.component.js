import React, { Component } from 'react';
import { Icon, Tag, Collapse } from 'antd';

import HttpService from '../../services/http.service';

require("storm-react-diagrams/dist/style.min.css");
import './style.scss';
import * as SRD from "storm-react-diagrams";

import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/styles/hljs';



class ProjectTemplate extends Component {
  constructor () {
    super();
    this.state = {
      selectedTab: 0,
      jsCode: "",
      textContent: "",
      collapseContent: [],
      media : ''
    }
  }

  componentDidMount () {
    //first time
    this.updateTextContent(this.props.detail.tabItems[this.state.selectedTab].content.text);
    this.setState({media: this.props.detail.media});
  }

  //reactive
  componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedTab !== this.state.selectedTab) {

      //load local txt if type is jscode during update
      if (this.props.detail.tabItems[this.state.selectedTab].type === 'jscode') {
        HttpService.simpleTextGet(this.props.detail.tabItems[this.state.selectedTab].content).then(data => this.setState({ jsCode: data }));
      }

      if (this.props.detail.tabItems[this.state.selectedTab].type === 'plain') {
        this.updateTextContent(this.props.detail.tabItems[this.state.selectedTab].content.text);
      }

      if (this.props.detail.tabItems[this.state.selectedTab].type === 'collapse') {
        const collapseContent = this.props.detail.tabItems[this.state.selectedTab].content.map((category, idx) => {
          const stacks = category.items.map((lib,idx2) => <Tag key={idx2}>{lib}</Tag>);
          return (
            <Collapse.Panel header={category.name} key={idx}>          
              {stacks}
            </Collapse.Panel>
          );
        });
        this.setState({ collapseContent }); 
      }
    }
  }

  onChangeDetailTab (selectedTab) {
    this.setState({ selectedTab });
  }



  updateTextContent (url) {
    HttpService.simpleTextGet(url).then(textContent => {
      let content = <div>
            <h2>{this.props.detail.tabItems[this.state.selectedTab].content.title}</h2>
            <section>{textContent}</section>
          </div>;
      this.setState({ textContent: content });
    });
  }


  render () {
    const subTabs = this.props.detail.tabItems.map((item,idx) => {
      return (
        <div className={`tab ` + (this.state.selectedTab == idx? 'selected':'')} key={idx} onClick={() => this.onChangeDetailTab(idx)}>
          <img src={item.image} />
          <span>{item.name}</span>
        </div>
      );
    });

    let currentContent = null;

    switch(this.props.detail.tabItems[this.state.selectedTab].type) {
      case 'plain': currentContent = this.state.textContent;
      break;
      case 'diagram': currentContent = <SRD.DiagramWidget className="srd-canvas" {...this.props.detail.tabItems[this.state.selectedTab].content} />;
      break;
      case 'jscode': currentContent = <SyntaxHighlighter language='javascript' showLineNumbers='true' style={docco}>{this.state.jsCode}</SyntaxHighlighter>;     
      break;
      case 'collapse': currentContent = <Collapse defaultActiveKey={['0']}>{this.state.collapseContent}</Collapse>
      break;
    }

    return (
      <div className="jing-project-container">
        <div className="jing-project-upper">
          <div className="jing-project-header">
            <div className="jing-project-meta">
              <div>
                <h3 className="sub-title">
                  {this.props.detail.subTitle}
                </h3>
                <h1 className="title">
                  {this.props.detail.title}
                </h1>
                <div className="note">
                  {this.props.detail.note}
                </div>
              </div>
              <div className="product" onClick={() => window.open(this.props.detail.productUrl, "_blank")}>
                {this.props.detail.productDescription}
              </div>
            </div>
          </div>
          <div className="jing-project-media">
            <video controls autoPlay>
              <source src= {this.state.media} type="video/mp4"/>
            </video>
          </div>
          <div className="jing-scroll-instruction">
            <span>Scroll Down for More Detail</span>
            <Icon type="down"/>
          </div>
        </div>
        <div className="jing-project-lower">
          <div className="tabs">
            {subTabs}
          </div>
          <div className="tab-content">
            {currentContent}
          </div>
        </div>
      </div>
    );
  }
}

export default ProjectTemplate;