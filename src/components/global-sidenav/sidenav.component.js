import React, { Component } from 'react';
import { withRouter, history } from "react-router-dom";

import { Menu, Icon } from 'antd';

import './style.scss';
const routeData = require('../../routes.json');

class GlobalSidenav extends Component {
  constructor(){
    super();
  }

  handleMenuClick (evt) {
    this.props.history.push(evt.key);
  }

  render () {
    /* dynamic import from json once it is fixed on Parcel */

    const menuItems = (
      <Menu
        defaultSelectedKeys={[routeData[0].pathname]}
        selectedKeys={[this.props.location.pathname]}
        mode="inline"
      >
        {routeData.map(route => {
          return (
            <Menu.Item key={route.pathname} onClick={this.handleMenuClick.bind(this)}>
            <Icon type="pie-chart"/>
            <span>{route.name}</span>
          </Menu.Item>
          );
        })}
      </Menu>
    );

    return (
      <div id="jing-nav-panel">
        <div id="title">
          Jing's React Space
        </div>
        {menuItems}
      </div>
    );
  }
}

export default withRouter(GlobalSidenav);