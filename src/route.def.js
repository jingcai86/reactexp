import React from 'react';

import { Route, HashRouter } from 'react-router-dom';

import GlobalSidenav from './components/global-sidenav/sidenav.component';
import WhereDemo from './routes/where/view';
import LocationScorecardDemo from './routes/locationscorecard/view';
import Boilerplate from './routes/boilerplate/view';
import Excel from './routes/excel/view';

export default class RouteDef extends React.Component {
    constructor(){
      super();
      this.state = { 
        routes: []
      };
    }
    
    componentDidMount() {
      /* Parcel dynamic import is not working yet 
      https://github.com/parcel-bundler/parcel/issues/112
      
      const routes = this.props.routeData.reduce(async (total,cur) => {
        total[cur.name] = await import(cur.location);
        return total;
      }, {});
      */
    }
    render () {
      return (
        <HashRouter>
          <div id="app-container">
            <GlobalSidenav></GlobalSidenav>
            <div id="app-content">
              <Route path='/where' component = {WhereDemo}/>
              <Route path='/locationscorecard' component = {LocationScorecardDemo}/>
              <Route path='/boilerplate' component = {Boilerplate}/>
              <Route path='/excel' component = {Excel}/>
            </div>
          </div>
        </HashRouter>
      );
    }
    
}